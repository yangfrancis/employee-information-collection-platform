"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.handleDoMain = exports.handleToken = void 0;
const utils_1 = require("../utils");
const Jwt_1 = require("../modules/Jwt");
const Config_1 = require("../modules/Config");
/**
 * 中间件-处理`token`验证
 * @param ctx
 * @param next
 * @description 需要`token`验证的接口时使用
 */
async function handleToken(ctx, next) {
    const checkInfo = Jwt_1.default.checkToken(ctx);
    if (checkInfo.fail) {
        ctx.body = checkInfo.info;
    }
    else {
        await next();
    }
}
exports.handleToken = handleToken;
/**
 * 中间件-处理域名请求：严格判断当前请求域名是否在白名单内
 * @param ctx
 * @param next
 */
async function handleDoMain(ctx, next) {
    const { referer, origin } = ctx.headers;
    // console.log(referer, origin);
    const domain = utils_1.default.getDomain(referer || "");
    const list = [...Config_1.default.origins, `http://${Config_1.default.ip}:${Config_1.default.port}`];
    // 严格判断当前请求域名是否在白名单内
    if (domain && list.indexOf(domain) > -1) {
        await next();
    }
}
exports.handleDoMain = handleDoMain;
//# sourceMappingURL=index.js.map