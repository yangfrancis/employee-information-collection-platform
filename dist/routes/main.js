"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Router = require("koa-router"); // learn: https://www.npmjs.com/package/koa-router
const Config_1 = require("../modules/Config");
/**
 * api路由模块
 */
const router = new Router({
    prefix: Config_1.default.apiPrefix
});
exports.default = router;
//# sourceMappingURL=main.js.map