"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fs = require("fs");
const path = require("path");
const main_1 = require("./main");
const utils_1 = require("../utils");
const apiResult_1 = require("../utils/apiResult");
const Config_1 = require("../modules/Config");
const request_1 = require("../utils/request");
/** 资源路径 */
const resourcePath = path.resolve(__dirname, "../../public/template");
const template = fs.readFileSync(resourcePath + "/page.html", "utf-8");
// "/*" 监听全部
main_1.default.get("/", (ctx, next) => {
    // 指定返回类型
    // ctx.response.type = "html";
    // ctx.response.type = "text/html; charset=utf-8";
    // const data = {
    //     pageTitle: "serve-root",
    //     jsLabel: "",
    //     content: `<button class="button button_green"><a href="/home">go to home<a></button>`
    // }
    // ctx.body = utils.replaceText(template, data);
    // console.log("根目录");
    // 路由重定向
    ctx.redirect("/home");
    // 302 重定向到其他网站
    // ctx.status = 302;
    // ctx.redirect("https://www.baidu.com");
});
main_1.default.get("/home", (ctx, next) => {
    const userAgent = ctx.header["user-agent"];
    ctx.response.type = "text/html; charset=utf-8";
    const data = {
        pageTitle: "serve-root",
        jsLabel: `<script src="https://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>`,
        content: `
        <div style="font-size: 24px; margin-bottom: 8px; font-weight: bold;">当前环境信息：</div>
        <p style="font-size: 15px; margin-bottom: 10px; font-weight: 500;">${userAgent}</p>
        <button class="button button_purple"><a href="./api-index.html">open test</></button>
        `
    };
    ctx.body = utils_1.default.replaceText(template, data);
    // console.log("/home");
});
// get 请求
main_1.default.get("/getData", (ctx, next) => {
    /** 接收参数 */
    const params = ctx.query || ctx.querystring;
    console.log("/getData", params);
    ctx.body = (0, apiResult_1.apiSuccess)({
        method: "get",
        port: Config_1.default.port,
        date: utils_1.default.formatDate()
    });
});
// post 请求
main_1.default.post("/postData", (ctx, next) => {
    /** 接收参数 */
    const params = ctx.request.body || ctx.params;
    console.log("/postData", params);
    const result = {
        data: "请求成功"
    };
    ctx.body = (0, apiResult_1.apiSuccess)(result, "post success");
});
// 请求第三方接口并把数据返回到前端
main_1.default.get("/getWeather", async (ctx, next) => {
    // console.log("ctx.query >>", ctx.query);
    const city = ctx.query.city;
    if (!city) {
        ctx.body = (0, apiResult_1.apiSuccess)({}, "缺少传参字段 city", 400);
        return;
    }
    const res = await (0, request_1.default)({
        method: "GET",
        hostname: "wthrcdn.etouch.cn",
        path: "/weather_mini?city=" + encodeURIComponent(city)
    });
    // console.log("获取天气信息 >>", res);
    if (res.state === 1) {
        if (utils_1.default.checkType(res.result) === "string") {
            res.result = JSON.parse(res.result);
        }
        ctx.body = (0, apiResult_1.apiSuccess)(res.result);
    }
    else {
        ctx.body = (0, apiResult_1.apiFail)(res.msg, 500, res.result);
    }
});
//# sourceMappingURL=test.js.map