"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const main_1 = require("./main");
const mysql_1 = require("../utils/mysql");
const apiResult_1 = require("../utils/apiResult");
const middleware_1 = require("../middleware");
const utils_1 = require("../utils");
const uuid = require('node-uuid');
const nodeExcel = require('excel-export');
// 获取所有技能信息列表
main_1.default.get("/getList", middleware_1.handleToken, async (ctx) => {
    const tokenInfo = ctx["theToken"];
    /** 返回结果 */
    let bodyResult;
    // console.log("getList >>", tokenInfo);
    // 这里要开始连表查询
    // const res = await query(`select * from user_skill_info where user_id = '${tokenInfo.id}'`)
    const res = await (0, mysql_1.default)(`select * from skill_info_table`);
    if (res.state === 1) {
        // console.log("/getList 查询", res.results);
        bodyResult = (0, apiResult_1.apiSuccess)({
            list: res.results.length > 0 ? res.results : []
        });
    }
    else {
        ctx.response.status = 500;
        bodyResult = (0, apiResult_1.apiFail)(res.msg, 500, res.error);
    }
    ctx.body = bodyResult;
});
// 添加列表
main_1.default.post("/addList", middleware_1.handleToken, async (ctx) => {
    const tokenInfo = ctx["theToken"];
    /** 接收参数 */
    const params = ctx.request.body;
    // 先查询是否有重复提交
    const result = await (0, mysql_1.default)(`select user_id from user_skill_info where user_id='${params.account}'`);
    if (result.state === 1) {
        if (result.results.length > 0) {
            return ctx.body = (0, apiResult_1.apiSuccess)(null, '重复提交', 405);
        }
    }
    else {
        ctx.response.status = 500;
        return (0, apiResult_1.apiFail)(result.msg, 500, result.error);
    }
    /** 返回结果 */
    let bodyResult = null;
    if (!params.content) {
        return ctx.body = (0, apiResult_1.apiSuccess)({}, "添加的列表内容不能为空！", 400);
    }
    const mysqlInfo = utils_1.default.mysqlFormatParams({
        "id": uuid.v1(),
        "level": params.level,
        "domain": params.domain,
        "location": params.location,
        "username": params.username,
        "language_skills": params.language_skills,
        "frontend_skills": params.frontend_skills,
        "backend_skills": params.backend_skills,
        "content": params.content,
        "user_id": tokenInfo.id,
        "update_time": utils_1.default.formatDate(),
        "create_time": utils_1.default.formatDate()
    });
    // 写入列表
    const res = await (0, mysql_1.default)(`insert into user_skill_info(${mysqlInfo.keys}) values(${mysqlInfo.symbols})`, mysqlInfo.values);
    // console.log("写入列表", res.results);
    if (res.state === 1) {
        bodyResult = (0, apiResult_1.apiSuccess)({
            id: res.results.insertId
        }, "添加成功", 200);
    }
    else {
        ctx.response.status = 500;
        bodyResult = (0, apiResult_1.apiFail)(res.msg, 500, res.error);
    }
    ctx.body = bodyResult;
});
// 修改列表
main_1.default.post("/modifyList", middleware_1.handleToken, async (ctx) => {
    /** 接收参数 */
    const params = ctx.request.body;
    /** 返回结果 */
    let bodyResult = null;
    if (!params.id) {
        return ctx.body = (0, apiResult_1.apiSuccess)({}, "列表id不能为空", 400);
    }
    if (!params.content) {
        return ctx.body = (0, apiResult_1.apiSuccess)({}, "列表内容不能为空", 400);
    }
    const setData = utils_1.default.mysqlSetParams({
        "content": params.content,
        "update_time": utils_1.default.formatDate()
    });
    // 修改列表
    const res = await (0, mysql_1.default)(`update user_skill_info ${setData} where id = '${params.id}'`);
    // console.log("修改列表", res);
    if (res.state === 1) {
        if (res.results.affectedRows > 0) {
            bodyResult = (0, apiResult_1.apiSuccess)({}, "修改成功");
        }
        else {
            bodyResult = (0, apiResult_1.apiSuccess)({}, "列表id不存在", 400);
        }
    }
    else {
        ctx.response.status = 500;
        bodyResult = (0, apiResult_1.apiFail)(res.msg, 500, res.error);
    }
    ctx.body = bodyResult;
});
// 删除列表
main_1.default.post("/deleteList", middleware_1.handleToken, async (ctx) => {
    // const state = ctx["theToken"];
    /** 接收参数 */
    const params = ctx.request.body;
    console.log('需要删除的账号', params);
    debugger;
    /** 返回结果 */
    let bodyResult = null;
    // 从数据库中删除
    // const res = await query(`delete from user_skill_info where id='${params.id}' and user_id='${state.info.id}'`)
    const res = await (0, mysql_1.default)(`delete from user_skill_info where user_id = '${params.id}'`);
    // const res = await query(`delete from user_skill_info where id in(${params.ids.toString()})`) // 批量删除
    // console.log("从数据库中删除", res);
    if (res.state === 1) {
        if (res.results.affectedRows > 0) {
            bodyResult = (0, apiResult_1.apiSuccess)({}, "删除成功");
        }
        else {
            bodyResult = (0, apiResult_1.apiSuccess)({}, "当前列表id不存在或已删除", 400);
        }
    }
    else {
        ctx.response.status = 500;
        bodyResult = (0, apiResult_1.apiFail)(res.msg, 500, res.error);
    }
    ctx.body = bodyResult;
});
// 获取所有员工列表
main_1.default.post("/getUserList", middleware_1.handleToken, async (ctx) => {
    const tokenInfo = ctx["theToken"];
    if (tokenInfo.id == null || tokenInfo.id == undefined)
        return (0, apiResult_1.apiSuccess)({}, 'token失效', 400);
    /** 接收参数 */
    const params = ctx.request.body;
    const size = Number(params.pageSize) || 10;
    const page = Number(params.currentPage) || 1;
    /** 返回结果 */
    let bodyResult;
    /** 精确查询 */
    // const accuracyText = utils.mysqlSearchParams({
    //     "level": params.level,
    //     "location": params.location,
    //     "language": params.language,
    //     "frontSkill": params.frontSkill,
    //     "backendList": params.backendList,
    //     "department": params.department
    // });
    // console.log('>>>>>>>>>>>>>>>', accuracyText.level)
    // debugger
    // 这里要开始连表查询
    let querySql = `select * from user_skill_info where 1=1`;
    if (params.level !== null && params.level !== '') {
        querySql += ` and level = '${params.level}'`;
    }
    if (params.location !== null && params.location !== '') {
        querySql += ` and location = '${params.location}'`;
    }
    if (params.language !== null && params.language !== '') {
        querySql += ` and language_skills like '%${params.language}%' `;
    }
    if (params.frontSkill !== null && params.frontSkill !== '') {
        querySql += ` and frontend_skills = '${params.frontSkill}' `;
    }
    if (params.department !== null && params.department !== '') {
        querySql += ` and department = '${params.department}' `;
    }
    if (params.backendList !== null && params.backendList !== '') {
        querySql += ` and backend_skills = '${params.backendList}' `;
    }
    if (params.username !== null && params.username !== '') {
        querySql += ` and username like '%${params.username}%' `;
    }
    querySql += ` limit ${size * (page - 1)}, ${size}`;
    // console.log(querySql)
    const res = await (0, mysql_1.default)(querySql);
    // params.level==null
    // const res = await query(`select * from user_skill_info where level=${params.level} limit ${size * (page - 1)}, ${size}`)
    // const resultText = `${params.level} order by create_time desc limit ${size * (page - 1)}, ${size}`;
    // const res = await query(`select * from user_skill_info ${resultText}`);
    if (res.state === 1) {
        // console.log("/getList 查询", res.results);
        bodyResult = (0, apiResult_1.apiSuccess)({
            list: res.results.length > 0 ? res.results : []
        });
    }
    else {
        ctx.response.status = 500;
        bodyResult = (0, apiResult_1.apiFail)(res.msg, 500, res.error);
    }
    ctx.body = bodyResult;
});
// 获取所有员工的excel
main_1.default.get("/getExcel", middleware_1.handleToken, async (req, res) => {
    let name = encodeURI('测试表');
    let conf = {};
    console.log("getList >>", req, res);
    conf.name = "aaa"; //表下面名字，默认不能为中文
    /**
     * 表头数据
     * */
    let colsArr = [
        {
            caption: '序号',
            type: 'number',
        },
        {
            caption: 'id',
            type: 'string',
        },
        {
            caption: 'name',
            type: 'string',
        },
    ];
    conf.cols = colsArr;
    let data = [
        {
            id: '1',
            name: 'zwh'
        }, {
            id: '222',
            name: 'dddd'
        }, {
            id: '839',
            name: '23ddg'
        }
    ];
    let array = [];
    for (var i = 0; i < data.length; i++) {
        var temp = new Array();
        temp[0] = i + 1;
        temp[1] = data[i].id;
        temp[2] = data[i].name;
        array.push(temp);
    }
    conf.rows = array;
    var result = nodeExcel.execute(conf);
    res.setHeader('Content-Type', 'application/vnd.openxmlformats;charset=utf-8');
    // res.setHeader("Content-Disposition", "attachment; filename=" + "Report.xlsx");
    res.setHeader("Content-Disposition", "attachment; filename=" + name + ".xlsx");
    res.end(result, 'binary');
});
//# sourceMappingURL=todo.js.map