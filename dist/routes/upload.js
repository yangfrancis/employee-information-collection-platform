"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fs = require("fs");
const path = require("path");
const main_1 = require("./main");
const Config_1 = require("../modules/Config");
const apiResult_1 = require("../utils/apiResult");
// 上传图片
// learn: https://www.cnblogs.com/nicederen/p/10758000.html
// learn: https://blog.csdn.net/qq_24134853/article/details/81745104
main_1.default.post("/uploadImg", async (ctx, next) => {
    const file = ctx.request.files[Config_1.default.uploadImgName];
    let fileName = ctx.request.body.name || `img_${Date.now()}`;
    fileName = `${fileName}.${file.name.split(".")[1]}`;
    // 创建可读流
    const render = fs.createReadStream(file.path);
    const filePath = path.join(Config_1.default.uploadPath, fileName);
    const fileDir = path.join(Config_1.default.uploadPath);
    if (!fs.existsSync(fileDir)) {
        fs.mkdirSync(fileDir);
    }
    // 创建写入流
    const upStream = fs.createWriteStream(filePath);
    render.pipe(upStream);
    // console.log(fileName, file);
    /** 模拟上传到七牛云 */
    function uploadApi() {
        const result = {
            image: ""
        };
        return new Promise(function (resolve) {
            const delay = Math.floor(Math.random() * 5) * 100 + 500;
            setTimeout(() => {
                result.image = `http://${Config_1.default.ip}:${Config_1.default.port}/images/${fileName}`;
                resolve(result);
            }, delay);
        });
    }
    const res = await uploadApi();
    ctx.body = (0, apiResult_1.apiSuccess)(res, "上传成功");
});
//# sourceMappingURL=upload.js.map