"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const main_1 = require("./main");
const mysql_1 = require("../utils/mysql");
const Jwt_1 = require("../modules/Jwt");
const middleware_1 = require("../middleware");
const apiResult_1 = require("../utils/apiResult");
const utils_1 = require("../utils");
const TableUser_1 = require("../modules/TableUser");
// var uuid = require('node-uuid');
// 注册
main_1.default.post("/register", async (ctx) => {
    /** 接收参数 */
    const params = ctx.request.body;
    /** 返回结果 */
    let bodyResult;
    /** 账号是否可用 */
    let validAccount = false;
    // console.log("注册传参", params);
    if (!/^[A-Za-z0-9]+$/.test(params.account)) {
        return ctx.body = (0, apiResult_1.apiSuccess)({}, "注册失败！账号必须由英文或数字组成", 400);
    }
    if (!/^[A-Za-z0-9]+$/.test(params.password)) {
        return ctx.body = (0, apiResult_1.apiSuccess)({}, "注册失败！密码必须由英文或数字组成", 400);
    }
    if (!params.name.trim()) {
        params.name = "用户未设置昵称";
    }
    // 先查询是否有重复账号
    const res = await (0, mysql_1.default)(`select account from user_table where account='${params.account}'`);
    console.log("注册查询", res);
    if (res.state === 1) {
        if (res.results.length > 0) {
            bodyResult = (0, apiResult_1.apiSuccess)(null, null, 405);
        }
        else {
            validAccount = true;
        }
    }
    else {
        ctx.response.status = 500;
        bodyResult = (0, apiResult_1.apiFail)(res.msg, 500, res.error);
    }
    // 再写入表格
    if (validAccount) {
        const mysqlInfo = utils_1.default.mysqlFormatParams({
            // "id": uuid.v1(),
            "id": Number(Date.now().toString().substring(7)),
            "account": params.account,
            "password": params.password,
            "name": params.name,
            "type": 0,
            "create_time": utils_1.default.formatDate()
        });
        // const res = await query(`insert into user_table(${mysqlInfo.keys}) values(${mysqlInfo.values})`) 这样也可以，不过 mysqlInfo.values 每个值都必须用单引号括起来，下面的方式就不用
        const res = await (0, mysql_1.default)(`insert into user_table(${mysqlInfo.keys}) values(${mysqlInfo.symbols})`, mysqlInfo.values);
        if (res.state === 1) {
            bodyResult = (0, apiResult_1.apiSuccess)(params, "注册成功");
        }
        else {
            ctx.response.status = 500;
            bodyResult = (0, apiResult_1.apiFail)(res.msg, 500, res.error);
        }
    }
    ctx.body = bodyResult;
});
// 登录
main_1.default.post("/login", async (ctx) => {
    /** 接收参数 */
    const params = ctx.request.body;
    /** 返回结果 */
    let bodyResult;
    // console.log("登录", params);
    if (!params.account || params.account.trim() === "") {
        return ctx.body = (0, apiResult_1.apiSuccess)({}, "登录失败！账号不能为空", 400);
    }
    if (!params.password || params.password.trim() === "") {
        return ctx.body = (0, apiResult_1.apiSuccess)({}, "登录失败！密码不能为空", 400);
    }
    // 先查询是否有当前账号
    const res = await (0, mysql_1.default)(`select * from user_table where account='${params.account}'`);
    // console.log("登录查询", res);
    if (res.state === 1) {
        // 再判断账号是否可用
        if (res.results.length > 0) {
            const data = res.results[0];
            // 最后判断密码是否正确
            if (data.password == params.password) {
                data.token = Jwt_1.default.createToken({
                    id: data.id,
                    account: data.account,
                    password: data.password,
                    type: data.type,
                    groupId: data.groupId,
                });
                bodyResult = (0, apiResult_1.apiSuccess)(data, "登录成功");
            }
            else {
                bodyResult = (0, apiResult_1.apiSuccess)({}, "密码不正确", 400);
            }
        }
        else {
            bodyResult = (0, apiResult_1.apiSuccess)({}, "该账号不存在，请先注册", 400);
        }
    }
    else {
        ctx.response.status = 500;
        bodyResult = (0, apiResult_1.apiFail)(res.msg, 500, res.error);
    }
    ctx.body = bodyResult;
});
// 获取用户信息
main_1.default.get("/getUserInfo", middleware_1.handleToken, async (ctx) => {
    const tokenInfo = ctx["theToken"];
    // /** 接收参数 */
    // const params = ctx.request.body;
    /** 返回结果 */
    let bodyResult;
    // console.log("getUserInfo >>", tokenInfo);
    const res = await (0, mysql_1.default)(`select * from user_table where account='${tokenInfo.account}'`);
    // console.log("获取用户信息 >>", res);
    if (res.state === 1) {
        // 判断账号是否可用
        if (res.results.length > 0) {
            const data = res.results[0];
            bodyResult = (0, apiResult_1.apiSuccess)(utils_1.default.objectToHump(data));
        }
        else {
            bodyResult = (0, apiResult_1.apiSuccess)({}, "该账号不存在，可能已经从数据库中删除", 400);
        }
    }
    else {
        ctx.response.status = 500;
        bodyResult = (0, apiResult_1.apiFail)(res.msg, 500, res.error);
    }
    ctx.body = bodyResult;
});
// 编辑用户信息
main_1.default.post("/editUserInfo", middleware_1.handleToken, async (ctx) => {
    const tokenInfo = ctx["theToken"];
    /** 接收参数 */
    const params = ctx.request.body;
    /** 返回结果 */
    let bodyResult;
    /** 账号是否可用 */
    let validAccount = false;
    // console.log("注册传参", params);
    if (!params.id) {
        ctx.response.status = 400;
        return ctx.body = (0, apiResult_1.apiSuccess)({}, "编辑失败！用户id不正确", 400);
    }
    if (!params.account || !/^[A-Za-z0-9]+$/.test(params.account)) {
        ctx.response.status = 400;
        return ctx.body = (0, apiResult_1.apiSuccess)({}, "编辑失败！账号必须由英文或数字组成", 400);
    }
    if (!params.password || !/^[A-Za-z0-9]+$/.test(params.password)) {
        ctx.response.status = 400;
        return ctx.body = (0, apiResult_1.apiSuccess)({}, "编辑失败！密码必须由英文或数字组成", 400);
    }
    if (utils_1.default.checkType(params.groupId) !== "number") {
        ctx.response.status = 400;
        return ctx.body = (0, apiResult_1.apiSuccess)({}, "编辑失败！分组不正确", 400);
    }
    if (!params.name.trim()) {
        params.name = "用户-" + utils_1.default.formatDate(Date.now(), "YMDhms");
    }
    if (TableUser_1.default.table[params.id]) {
        validAccount = true;
        for (const key in TableUser_1.default.table) {
            if (Object.prototype.hasOwnProperty.call(TableUser_1.default.table, key)) {
                const item = TableUser_1.default.table[key];
                if (item.account == params.account && item.id != params.id) {
                    validAccount = false;
                    bodyResult = (0, apiResult_1.apiSuccess)({}, "当前账户已存在", -1);
                    break;
                }
            }
        }
    }
    else {
        bodyResult = (0, apiResult_1.apiSuccess)({}, "当前用户 id 不存在", -1);
    }
    // 再写入表格
    if (validAccount) {
        const setData = utils_1.default.mysqlSetParams({
            "account": params.account,
            "password": params.password,
            "name": params.name,
            "type": params.type,
            "group_id": params.groupId
        });
        // console.log("修改用户信息语句 >>", `update user_table ${setData} where id = '${params.id}'`);
        const res = await (0, mysql_1.default)(`update user_table ${setData} where id = '${params.id}'`);
        // console.log("再写入表格 >>", res);
        if (res.state === 1) {
            const data = {};
            TableUser_1.default.updateById(params.id, {
                password: params.password,
                name: params.name,
                type: params.type,
                groupId: params.groupId,
            });
            // 判断是否修改自己信息
            if (params.id == tokenInfo.id) {
                data.token = Jwt_1.default.createToken({
                    id: params.id,
                    account: params.account,
                    password: params.password,
                    type: params.type,
                    groupId: params.groupId,
                });
            }
            bodyResult = (0, apiResult_1.apiSuccess)(data, "编辑成功");
        }
        else {
            ctx.response.status = 500;
            bodyResult = (0, apiResult_1.apiFail)(res.msg, 500, res.error);
        }
    }
    ctx.body = bodyResult;
});
// // 获取用户列表
// router.get("/getUserList", handleToken, async (ctx: TheContext) => {
//     const tokenInfo = ctx["theToken"];
//     // console.log("tokenInfo >>", tokenInfo);
//     const params: UserListParams = ctx.request.query as any;
//     const size = Number(params.pageSize) || 10;
//     const page = Number(params.currentPage) || 1;
//     /** 精确查询 */
//     const accuracyText = utils.mysqlSearchParams({
//         "type": params.type
//     });
//     /** 模糊查询 */
//     const vagueText = utils.mysqlSearchParams({
//         "name": params.name
//     }, true)
//     /** 查询语句 */
//     const searchText = (function() {
//         let result = "";
//         if (params.groupId) {
//             result += utils.mysqlFindInSet("group_ids", [params.groupId]);
//         } else {
//             if ((tokenInfo.type >= 5)) {
//                 result += utils.mysqlFindInSet("group_ids", tokenInfo.groupIds.split(","));
//             }
//         }
//         if (accuracyText) {
//             result += `${result ? " and " : ""}${accuracyText}`;
//         }
//         if (vagueText) {
//             result += `${result ? " and " : ""}${vagueText}`;
//         }
//         if (result) {
//             result = `where ${result}`;
//         }
//         return result;
//     })();
//     /** 结果语句 */
//     const resultText = `${searchText} order by create_time desc limit ${size * (page - 1)}, ${size}`;
//     // const res = await query(`select * from user_table`)
//     const resultCountText = `select count(*) from user_table ${searchText.replace(/t2./g, "")}`;
//     const countRes = await query(resultCountText)
//     // console.log(selectUserTable + resultText);
//     // "select * from user_table" + resultText
//     // selectUserTable + resultText
//     // console.log("用户查询语句 >>", `select * from user_table ${resultText}`);
//     const res = await query(`select * from user_table ${resultText}`)
//     // console.log("获取用户列表 >>", res);
//     /** 返回结果 */
//     let bodyResult: ApiResult<any>;
//     if (res.state === 1) {
//         const list: Array<UserInfo> = res.results || [];
//         const result = [];
//         for (let i = 0; i < list.length; i++) {
//             const item = list[i];
//             if (item.type < tokenInfo.type) {
//                 item.password = "******";
//             }
//             // 这里可以做分组名设置
//             group.matchGroupIds(item);
//             result.push(user.matchName(item as any));
//         }
//         bodyResult = apiSuccess({
//             pageSize: size,
//             currentPage: page,
//             total: countRes.results[0][`count(*)`],
//             list: result,
//             time: Date.now()
//         });
//     } else {
//         ctx.response.status = 500;
//         bodyResult = apiFail(res.msg, 500, res.error);
//     }
//     ctx.body = bodyResult;
// })
// 删除用户
main_1.default.post("/deleteUser", middleware_1.handleToken, async (ctx) => {
    const tokenInfo = ctx["theToken"];
    /** 接收参数 */
    const params = ctx.request.body;
    // console.log(params);
    if (tokenInfo && tokenInfo.type != 0) {
        return ctx.body = (0, apiResult_1.apiSuccess)({}, "当前账号没有权限删除用户", -1);
    }
    if (!params.id) {
        ctx.response.status = 400;
        return ctx.body = (0, apiResult_1.apiSuccess)({}, "编辑失败！用户id不正确", 400);
    }
    /** 返回结果 */
    let bodyResult;
    // 从数据库中删除
    const res = await (0, mysql_1.default)(`delete from user_table where id = '${params.id}'`);
    // console.log("获取用户列表 >>", res);
    if (res.state === 1) {
        if (res.results.affectedRows > 0) {
            bodyResult = (0, apiResult_1.apiSuccess)({}, "删除成功");
            TableUser_1.default.remove(params.id);
            // 异步删除所有关联到的表单数据即可，不需要等待响应
            // query(`delete from street_shop_table where user_id='${params.id}'`)
        }
        else {
            bodyResult = (0, apiResult_1.apiSuccess)({}, "当前列表id不存在或已删除", 400);
        }
    }
    else {
        ctx.response.status = 500;
        bodyResult = (0, apiResult_1.apiFail)(res.msg, 500, res.error);
    }
    ctx.body = bodyResult;
});
// 退出登录
main_1.default.get("/logout", middleware_1.handleToken, ctx => {
    const token = ctx.header.authorization;
    if (token) {
        return ctx.body = (0, apiResult_1.apiSuccess)({}, "退出登录成功");
    }
    else {
        return ctx.body = (0, apiResult_1.apiSuccess)({}, "token 不存在", 400);
    }
});
//# sourceMappingURL=user.js.map