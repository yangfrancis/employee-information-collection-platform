"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.apiFail = exports.apiSuccess = void 0;
/**
 * 接口成功响应返回数据
 * @param data 成功返回数据
 * @param tip 提示内容
 * @param code 状态码
 */
function apiSuccess(data, tip, code = 200) {
    return {
        message: tip || "success",
        code: code,
        result: data
    };
}
exports.apiSuccess = apiSuccess;
/**
 * 服务端错误响应返回数据
 * @param tip 提示内容
 * @param code 错误码
 * @param error 错误信息
 */
function apiFail(tip, code = 500, error = null) {
    return {
        message: tip || "fail",
        code: code,
        result: error
    };
}
exports.apiFail = apiFail;
//# sourceMappingURL=apiResult.js.map