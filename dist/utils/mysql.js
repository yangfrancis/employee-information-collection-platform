"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mysql = require("mysql"); // learn: https://www.npmjs.com/package/mysql
const Config_1 = require("../modules/Config");
/** 数据库链接池 */
const pool = mysql.createPool({
    host: Config_1.default.db.host,
    user: Config_1.default.db.user,
    password: Config_1.default.db.password,
    database: Config_1.default.db.database
});
/**
 * 数据库增删改查
 * @param command 增删改查语句 [mysql语句参考](https://blog.csdn.net/gymaisyl/article/details/84777139)
 * @param value 对应的值
 */
function query(command, value) {
    const result = {
        state: 0,
        results: null,
        fields: null,
        error: null,
        msg: ""
    };
    return new Promise((resolve, reject) => {
        pool.getConnection((error, connection) => {
            if (error) {
                result.error = error;
                result.msg = "数据库连接出错";
                resolve(result);
            }
            else {
                const callback = (error, results, fields) => {
                    // pool.end();
                    connection.release();
                    if (error) {
                        result.error = error;
                        result.msg = "数据库增删改查出错";
                        resolve(result);
                    }
                    else {
                        result.state = 1;
                        result.msg = "ok";
                        result.results = results;
                        result.fields = fields;
                        resolve(result);
                    }
                };
                if (value) {
                    pool.query(command, value, callback);
                }
                else {
                    pool.query(command, callback);
                }
            }
        });
    });
}
exports.default = query;
//# sourceMappingURL=mysql.js.map