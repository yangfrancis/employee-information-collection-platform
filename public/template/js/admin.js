// 类型提示用（运行时不会引用）
/// <reference path="./api.js" />

// 用户信息
const userInfo = fetchUserInfo()

search({ page: 1, pageSize: 20 });

function outTablelist(data) {
  layui.use('table', function(){
    var table = layui.table; 
    //第一个实例
    table.render({
      elem: '#searchtable',
      height: 500,
      page: true, //开启分页
      cols: [[ //表头
        // {field: 'id', title: 'ID', width:80, sort: true, fixed: 'left'},
        {field: 'user_id', title: 'id', width:80, },
        {field: 'username', title: '用户名', width:180, },
        {field: 'level', title: '级别', width:120 },
        {field: 'location', title: 'Base区域', width:120},
        {field: 'domain', title: '部门', width: 120 },
        {field: 'language_skills', title: '语言技能', width: 150 },
        {field: 'frontend_skills', title: '前端技能', width: 150},
        {field: 'backend_skills', title: '后端技能', width: 150 },
        // {field: 'create_time', title: '创建时间', width: 80, sort: true},
        // {field: 'update_time', title: '更新时间', width: 80, sort: true},
        {field: 'content', title: '备注', width: 200 },
      ]],
      data: data.map(i => {
        i.language_skills = Object.values(JSON.parse(i.language_skills)).filter(i=>i !== '').join(',')
        return i;
      })
    });
    
  });
}

// 导出excel
function exportexcel(){
  api.exportDataToExcel()
}

// 查询信息Table入口
function search(params){
  let frontForm = layui.form.val('frontSkill')
  let backendForm = layui.form.val('backendSkill')
  let languag = layui.form.val('language')
  let department = layui.form.val('department')
  let grade = layui.form.val('level')
  let location = layui.form.val('location')
  let username = layui.form.val('username')
  const data = { 
    frontSkill:frontForm.frontSkill,
    backendList:backendForm.backendSkill, 
    department: department.department,
    level:grade.level,
    language:languag.language,
    location: location.location, 
    username: username.username,
    ...params
  };
  
  api.getUserList(
    data,
    (res) => {
      outTablelist(res.result.list);
    }
  )
}

