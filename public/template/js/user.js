// 类型提示用（运行时不会引用）
/// <reference path="./api.js" />

const userInfo = fetchUserInfo()

function openUserPage() {
  location.href = './api-login.html'
}

// if (!userInfo) {
//     openUserPage();
// }
let element, form, formObj
layui.use(['element', 'form', 'layer'], function () {
  ;(element = layui.element), (form = layui.form)
  // form.on('submit(formDemo)', function (data) {
  //   console.log(data)
  //   layer.msg(JSON.stringify(data.field))
  //   return false
  // })
})

function submit() {
  let frontForm = form.val('frontSkill')
  let frontList = Object.keys(frontForm)
  let backendForm = form.val('backendSkill')
  let backendList = Object.keys(backendForm)
  let languag = form.val('language')
  let department = form.val('department')
  let grade = form.val('grade')
  let username = form.val('username') 
  let location = form.val('location')
  let remark = form.val('remark')
  formObj = {
    frontList,
    languag,
    backendList,
    level: grade,
    username: username.username,
    domain: department,
    location: location,
    content: remark,
    account: userInfo.id,
  }
  if(formObj.languag === ''|| formObj.username === '') {
    return utils.showAlert({ content: '请完整填写信息' })
  }
  api.addListItem(formObj, res => {
    if(+res.code===200){
      utils.showToast('提交成功');
    }
  })
}
/**
 * 获取二进制路径（需要打开服务器调试）
 * @param {File} file 文件
 */
function getObjectURL(file) {
  let url = null
  if (window.createObjectURL) {
    url = window.createObjectURL(file)
  } else if (window.URL) {
    url = window.URL.createObjectURL(file)
  } else if (window.webkitURL) {
    url = window.webkitURL.createObjectURL(file)
  }
  return url
}

/**
 * 上传图片
 * @param {HTMLInputElement} el
 */
function uploadImg(el) {
  /** 上传文件 */
  const file = el.files[0]
  /** 上传类型数组 */
  const types = ['image/jpg', 'image/png', 'image/jpeg', 'image/gif']
  // 判断文件类型
  if (types.indexOf(file.type) < 0)
    return utils.showAlert({ content: '文件格式只支持：jpg 和 png' })
  // 判断大小
  if (file.size > 2 * 1024 * 1024)
    return utils.showAlert({ content: '上传的文件不能大于2M' })

  const formData = new FormData()
  // formData.append("name", "hjs-img");
  formData.append('img', file)
  // console.log(formData);

  api.upload(
    formData,
    (res) => {
      console.log('上传成功', res)
      el.parentNode.classList.add('hide')
      el.parentNode.parentNode
        .querySelector('.img-box')
        .classList.remove('hide')
      el.parentNode.parentNode.querySelector('.img-box .image').src =
        res.result.image
    },
    (err) => {
      console.log('上传失败', err)
    }
  )

  el.value = null
}

/**
 * 清除图片
 * @param {HTMLElement} el
 */
function removeImg(el) {
  el.parentNode.classList.add('hide')
  el.parentNode.querySelector('.image').src = ''
  el.parentNode.parentNode.querySelector('.upload').classList.remove('hide')
}

/** 列表节点 */
const langlistEl = utils.find('#list-item-language')

/** 模板内容 */
const template1 = langlistEl.children[0].innerHTML

// 清空列表
langlistEl.innerHTML = null

/** 列表节点 */
const forntlistEl = utils.find('#list-item-front')
// /** 模板内容 */
const template2 = forntlistEl.children[0].innerHTML

/** 列表节点 */
const backendlistEl = utils.find('#list-item-backend')
// /** 模板内容 */
const template3 = backendlistEl.children[0].innerHTML
/**
 * 输出列表item
 * @param {{ content: string, id: number }} item
 */
function ouputList(item) {
  JSON.parse(item.language_skills).forEach((k) => {
    const itemHTML = template1
      .replace('{{language}}', k.language)
      .replace('{{key}}', k.key)
    langlistEl.insertAdjacentHTML('beforeend', itemHTML)
  })
  JSON.parse(item.frontend_skills).forEach((k) => {
    const htmltemplate = template2
      .replace('{{frame}}', k.frame)
      .replace('{{key}}', k.key)
    forntlistEl.insertAdjacentHTML('beforeend', htmltemplate)
    // forntlistEl.insertAdjacentText('beforeend', htmltemplate)
  })
  JSON.parse(item.backend_skills).forEach((k) => {
    const htmltemplate = template3
      .replace('{{frame}}', k.frame)
      .replace('{{key}}', k.key)
    backendlistEl.insertAdjacentHTML('beforeend', htmltemplate)
    // forntlistEl.insertAdjacentText('beforeend', htmltemplate)
  })
  form.render()
}

/**
 * 增加一条列表
 * @param {HTMLElement} el
 */
function addList(el) {
  /**
   * @type {HTMLInputElement}
   */
  const input = el.parentNode.querySelector('.input')
  const text = input.value.trim()
  if (!text) return utils.showAlert({ content: '输入的内容不能为空~' })
  api.addListItem(text, (res) => {
    ouputList({
      content: text,
      id: res.result.id,
    })
    input.value = null
  })
}

/**
 * 删除当前列表
 * @param {HTMLElement} el
 */
function removeList(el) {
  // return console.log(el.parentNode.dataset["id"]);
  api.deleteListItem(el.parentNode.dataset['id'], (res) => {
    console.log('删除成功', res)
    utils.showToast('删除成功')
    el.parentNode.parentNode.removeChild(el.parentNode)
  })
}

/**
 * 修改当前列表内容
 * @param {HTMLElement} el 自身节点
 */
function subChange(el) {
  let id = el.parentNode.dataset['id']
  let text = el.parentNode.querySelector('.input').value.trim()
  if (!text) return utils.showAlert({ content: '内容不能为空' })
  // console.log(text, id);
  api.modifyListItem(
    {
      content: text,
      id: id,
    },
    (res) => {
      console.log('修改成功', res)
      utils.showToast('修改成功')
      offInput(el)
    }
  )
}

/**
 * 使输入框可以修改
 * @param {HTMLElement} el 自身节点
 */
function canInput(el) {
  el.parentNode.querySelector('.button_blue').classList.remove('hide')
  el.classList.add('hide')
  el.parentNode.querySelector('.input').removeAttribute('readonly')
}

/**
 * 使输入框不可以修改
 * @param {HTMLElement} el 自身节点
 */
function offInput(el) {
  el.parentNode.querySelector('.button_blue').classList.add('hide')
  el.parentNode.querySelector('.button_green').classList.remove('hide')
  el.parentNode.querySelector('.input').setAttribute('readonly', 'readonly')
}

api.getTodoList(
  (res) => {
    if (res.result.list.length == 0) return
    res.result.list.forEach((item) => {
      ouputList(item)
    })
  },
  (err) => {}
)

function clickGetUserInfo() {
  api.getUserInfo((res) => {
    console.log('用户信息', res)
  })
}

function clickLogout() {
  api.logout(
    (res) => {
      console.log('退出登录', res)
      removeUserInfo()
      openUserPage()
    },
    () => {
      removeUserInfo()
    }
  )
}

function reset() {
  const layForm = document.querySelectorAll('.layui-form')
  Array.prototype.forEach.call(layForm, (item) => item.reset())
  api.deleteListItem(userInfo.id, (res) => {
    console.log('删除成功', res)
    utils.showToast('删除成功')
    el.parentNode.parentNode.removeChild(el.parentNode)
  })
}
